global class AccRecordUpdate implements Database.Batchable<sObject> 
{
        global Database.QueryLocator start(Database.BatchableContext bc)
    {
        String query= 'SELECT id, RecordType.Name, Gold_Account__c, Enterprise_Account_Status__c from Account where RecordType.Name = \'Customer Account\'';
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext bc, List<Account> scope)
    {
        for(Account acc: scope)
        {
            if(acc.Gold_Account__c == false)      
                acc.Enterprise_Account_Status__c ='Bronze'; 
            else
                acc.Enterprise_Account_Status__c ='Gold'; 
        }
        update scope;
    }
    global void finish(Database.BatchableContext bc){}
}