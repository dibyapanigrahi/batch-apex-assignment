Hi,

Please find the details here.
-------------------------------------------------------------------------

Class: AccRecordUpdate
Test Class: AccRecordUpdate_test
Code Coverage for the class: 100%

To run the class, open Developer console
then Debug --> Open Execute Anonymous Window.

There paste the following line 
Id batchJobId = Database.executeBatch(new AccRecordUpdate(), 200);

-------------------------------------------------------------------------

To check the status of the job, please go to 
setup --> Search 'Apex Jobs' in quick search box in left panel. 

-------------------------------------------------------------------------

I have covered the following conditions as outlined in the problem statement. 

Object: Accounts 
Record Type: Customer Account  
Custom field: Enterprise_Account_Status__c = Bronze.

Also if Custom field: Gold_Account__c = true, then 
Custom field: Enterprise_Account_Status__c = Gold.

I haven't put email confirmation method in the finish method as it was not 
part of the problem statement. 

-------------------------------------------------------------------------

The code block is not difficult to change as per the new requirement.
This code can't be made completely generic. 

In Salesforce schema used at your company, there could be so many
1. Objects (Standard and Custom),
2. Fields (Standard and Custom),
3. RecordTypes
4. and multiple other conditions. 

So I believe that the code block I have written would act as a template for 
similar requirements with first 3 variables mentioned above. 

-------------------------------------------------------------------------
 