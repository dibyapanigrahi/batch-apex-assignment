@isTest
public class AccRecordUpdate_test
{
    static testMethod void Atlassian()
    {
        List<Account> AccList = new List<Account>();
        Id CustAccRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer Account').getRecordTypeId();
        for(Integer i=0; i < 170; i++)
        {
            Account acc = new Account();
            acc.Name = 'Recent Account ' + i + 1; 
            acc.RecordtypeId = CustAccRT;
            AccList.add(acc);
        }
        for(Integer i=170 ;i <200; i++)
        {
            Account goldAcc = new Account();
            goldAcc.Name = 'Recent Account ' + i + 1; 
            goldAcc.RecordtypeId = CustAccRT;
            goldAcc.Gold_Account__c = true;
            AccList.add(goldAcc);
        }
        database.insert (AccList, false);
        Test.startTest();
            AccRecordUpdate obj = new AccRecordUpdate();
            DataBase.executeBatch(obj);
        Test.stopTest();
    }
}